#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: msans
"""
import sys
import argparse
import pathlib
import time
from datetime import date
from datetime import timedelta
import solaredge
import pandas as pd
import json
import tomli


class SolarEdgeMonitor:
    """SolarEdge inverter monitoring class"""
    def __init__(self, user_data_file: str, data_storage_folder: str):

        self.err = False
        self.err_msg = ''

        self.data_storage_folder = data_storage_folder
        self.admin_info = dict()
        self.data_period = dict()
        self.current_power_flow = dict()
        self.overview_power_production = dict()

        self.api_key = None
        self.site_id = None
        self._get_user_data(user_data_file)

    def _get_user_data(self, user_data_file: str) -> dict:
        """Get user data (API key and Site ID) from a json file"""
        p = pathlib.Path(user_data_file)
        p = p.expanduser()
        p = p.resolve()

        try:
            with open(p, 'rb') as f:
                user_data = tomli.load(f)
        except (IOError, tomli.TOMLDecodeError) as err:
            self.err_msg = f"ERROR: could not read config file {p}: " + str(err)
            self.err = True
            return self.err
        try:
            self.api_key = user_data['api_key']
            self.site_id = int(user_data['site_id'])
        except (KeyError) as err:
            self.err_msg = f"ERROR: reading config file {p}: " + str(err)
            self.err = True
            return self.err
        self.solaredge = solaredge.Solaredge(self.api_key)
        return self.err

    def _manage_time_interval(self, start_date: str, period: str, use_operation_start: bool,
                             end_date: str, use_operation_end: bool) -> list:
        """Check bounds of the time interval"""
        oneday = timedelta(days=1)
        p = int(period)
        if use_operation_end:
            end_date = str(date.fromisoformat(self.data_period['endDate']) - oneday)
        else:
            x = date.fromisoformat(end_date)
            if x > date.fromisoformat(self.data_period['endDate']):
                end_date = str(date.fromisoformat(self.data_period['endDate']) - oneday)
            elif x < date.fromisoformat(self.data_period['startDate']):
                end_date = str(date.fromisoformat(self.data_period['startDate']) + oneday)
            else:
                pass

        if use_operation_start:
            start_date = str(date.fromisoformat(self.data_period['startDate']) + oneday)
        else:
            x = date.fromisoformat(start_date)
            if p > 0:
                period_days = timedelta(days=p)
                x = date.fromisoformat(end_date) - period_days
            if x > date.fromisoformat(self.data_period['endDate']):
                start_date = str(date.fromisoformat(self.data_period['endDate']) - oneday)
            elif x < date.fromisoformat(self.data_period['startDate']):
                start_date = str(date.fromisoformat(self.data_period['startDate']) + oneday)
            else:
                start_date = str(x)

        if date.fromisoformat(start_date) > date.fromisoformat(end_date):
            start_date = end_date

        return start_date, end_date

    def _get_raw_data(self, day: str) -> dict:
        """Ask solaredge server for the raw enery and power data"""
        start_time = day + ' 00:00:00'
        stop_time = day + ' 23:59:59'
        time.sleep(0.5)
        data = dict()
        try:
            edata = self.solaredge.get_energy_details(self.site_id, start_time, stop_time,
                                                      meters=None, time_unit='QUARTER_OF_AN_HOUR')
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = "ERROR: could not get energy data: " + err
            self.err = True
            return data
        time.sleep(0.5)
        try:
            pdata = self.solaredge.get_power_details(self.site_id, start_time, stop_time, meters=None)
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = "ERROR: could not get power data: ", + err
            self.err = True
            return data
        data['energy'] = edata
        data['power'] = pdata
        return data

    def _format_data(self, raw_data: dict) -> dict:
        """Format raw energy and power data into a dictionary, suitable for json file storage"""
        data = dict()

        data['validity'] = True
        data['power_unit'] = raw_data['power']['powerDetails']['unit']
        data['energy_unit'] = raw_data['energy']['energyDetails']['unit']

        data['time_resolution'] = raw_data['energy']['energyDetails']['timeUnit']
        data['time_unit'] = 'h'
        data['timestamps'] = list()
        [data['timestamps'].append(k['date']) for k in raw_data['energy']['energyDetails']['meters'][0]['values']]
        data['timegrid'] = list()
        for k in data['timestamps']:
            t = time.strptime(k, "%Y-%m-%d %H:%M:%S")
            data['timegrid'].append(t.tm_hour + t.tm_min / 60 + t.tm_sec / 3600)

        data['energy'] = dict()
        data['power'] = dict()

        for key, val in {'power': 'powerDetails', 'energy': 'energyDetails'}.items():
            for k in raw_data[key][val]['meters']:
                data[key][k['type']] = list()
                for n in k['values']:
                    if 'value' in n:
                        data[key][k['type']].append(n['value'])
                    else:
                        data[key][k['type']].append(0.0)
                        if k['type'] != 'Production':
                            data['validity'] = False

        return data

    def _check_data(self, data: dict) -> bool:
        """Check if energy and power data contain any NaN value"""
        for k in ['energy', 'power']:
            for kk in data[k]:
                if True in pd.isna(data[k][kk]):
                    return True
        return False

    def _dump_2_json(self, filename: str, data: dict):
        try:
            with open(filename, "w") as outfile:
                json.dump(data, outfile)
        except IOError as err:
            self.err_msg = f"ERROR: could not write {filename}:" + str(err)
            self.err = True
            return True
        return self.err

    def get_data(self, start_date: str, period: str, use_operation_start: bool,
                 end_date: str, use_operation_end: bool, refresh_data: bool,
                 max_fetch_attempt: str, quiet: bool) -> bool:
        """Ask solaredge server for energy and power data for the specified date range"""
        self.get_data_period(False)
        if self.err:
            return True
        start_date, end_date = self._manage_time_interval(start_date, period, use_operation_start, end_date, use_operation_end)
        day_list = (pd.date_range(start=start_date, end=end_date)).strftime('%Y-%m-%d')

        for d in day_list:
            p = pathlib.Path(pathlib.Path(self.data_storage_folder), pathlib.Path(d + ".json"))

            # skip existing valid data file if no refresh required
            if not refresh_data and p.is_file():
                if not (quiet):
                    sys.stdout.write(f"Fetching {d} data ... SKIPPED (no refresh required)\n")
                continue

            # get and format raw data
            fetch_count_max = int(max_fetch_attempt)
            fetch_count = fetch_count_max
            data = dict()
            while fetch_count > 0:
                if not(quiet):
                    sys.stdout.write(
                        f"Fetching {d} data (attempt {fetch_count_max-fetch_count+1}/{fetch_count_max}) ...")
                raw_data = self._get_raw_data(d)
                if self.err:
                    return True
                data = self._format_data(raw_data)
                if self._check_data(data):
                    fetch_count -= 1
                    continue
                if not (quiet):
                    sys.stdout.write(" DONE\n")
                break

            if fetch_count == 0:
                self.err_msg = "ERROR: solaredge server is returning NaN data. Aborting."
                self.err = True
                return True
            # dump
            self._dump_2_json(p, data)
            if self.err:
                return True

        return False

    def get_data_period(self, dump: bool) -> bool:
        """Ask solaredge server for exploitation start date and last date"""
        try:
            raw_data_period = self.solaredge.get_data_period(self.site_id)
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = ("ERROR: could not get exploitation start and last dates: ", err)
            self.err = True
            return self.err

        try:
            self.data_period = raw_data_period['dataPeriod']
        except KeyError as err:
            self.err_msg = ("ERROR: could not find dataPeriod: ", err)
            self.err = True
            return self.err

        if dump:
            p = p = pathlib.Path(self.data_storage_folder, 'data_period.'+str(self.site_id)+'.json')
            self._dump_2_json(p,self.data_period)
            if self.err:
                return True

        return self.err

    def get_admin_info(self) -> bool:
        """Ask solaredge server for site admin information data"""
        try:
            raw_admin_info = self.solaredge.get_list()
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = "ERROR: could not get site admin information: " + str(err)
            self.err = True
            return True

        for s in raw_admin_info['sites']['site']:
            if s['id'] == self.site_id:
                self.admin_info = s
                p = pathlib.Path(self.data_storage_folder, 'admin_info.'+str(self.site_id)+'.json')
                self._dump_2_json(p,self.admin_info)
                if self.err:
                    return True
                return self.err
        self.err_msg = "ERROR: unexpected error: site ID does not match in the server answer !"
        self.err = True
        return True

    def get_current_power_flow(self) -> bool:
        """Ask solaredge server for the current power flow data"""
        try:
            raw_current_power_flow = self.solaredge.get_current_power_flow(self.site_id)
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = "ERROR: could not get current power flow data: " + str(err)
            self.err = True
            return self.err

        try:
            self.current_power_flow = raw_current_power_flow['siteCurrentPowerFlow']
        except KeyError as err:
            self.err_msg = ("ERROR: could not find raw_current_power_flow: ", err)
            self.err = True
            return self.err

        p = pathlib.Path(self.data_storage_folder, 'current_power_flow.' + str(self.site_id) + '.json')
        self._dump_2_json(p, self.current_power_flow)
        if self.err:
            return True

        return self.err

    def get_overview_power_production(self) -> bool:
        """Ask solaredge server for an overview of the power production"""
        try:
            raw_overview_power_production = self.solaredge.get_overview(self.site_id)
        except BaseException as err:  # catch any error raised by solaredge module
            self.err_msg = "ERROR: could not get the overview of the power production: " + str(err)
            self.err = True
            return self.err

        try:
            self.overview_power_production = raw_overview_power_production['overview']
        except KeyError as err:
            self.err_msg = ("ERROR: could not find overview: ", err)
            self.err = True
            return self.err

        p = pathlib.Path(self.data_storage_folder, 'power_production_overview.' + str(self.site_id) + '.json')
        self._dump_2_json(p, self.overview_power_production)
        if self.err:
            return True

        return self.err

    def is_error(self):
        return self.err

    def get_error(self):
        return self.err_msg

def solaredgemon() -> int:
    """Monitor solaredge inverter data"""
    # get current date
    oneday = timedelta(days=1)
    today = date.today()
    yesterday = today - oneday

    # argument parsing
    script_path = pathlib.Path(__file__).absolute()
    usage = "%(prog)s [options] [USERDATAFILE]"
    description = script_path.name + " is a wrapper for solaredge module"
    parser = argparse.ArgumentParser(usage=usage, description=description)

    parser.add_argument("-q", "--quiet", dest="quiet", action='store_true',
                        help="Quiet mode.")
    parser.add_argument("-A", "--admin-info", dest="admin_info", action='store_true',
                        help="Get admin info")
    parser.add_argument("-D", "--data-period", dest="data_period", action='store_true',
                        help="Get data period. Ignored if -A is used.")
    parser.add_argument("-O", "--overview-production", dest="overview", action='store_true',
                        help="Get power production overview. Ignored if -A, or -O is used.")
    parser.add_argument("-P", "--current-power", dest="current_power", action='store_true',
                        help="Get current power flow. Ignored if -A,-D or -O is used.")
    parser.add_argument("-s", "--start-date", dest="start_date", default=None, action='store', type=str,
                        help="Start date for data retrieval (included). Date format is YYYY-MM-DD." + \
                             " Ignored if -A, -D, -O or -P is used.")
    parser.add_argument("-p", "--period", dest="period", default="0", action='store', type=int,
                        help="Integer number of day that defines the period for data retrieval. "
                             + "The start date is defined by subtracting this number from end date."
                             + "If defined, -s is ignored. Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-S", "--use-operation-start", dest="use_operation_start", action='store_true',
                        help="Use the first day of operation (first day with complete data) "
                             + "as start date for data retrieval. "
                             + "If set, -s and -p are ignored. Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-e", "--end-date", dest="end_date", default=str(yesterday), action='store', type=str,
                        help="End date for data retrieval (included). Date format is YYYY-MM-DD." + \
                             " Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-E", "--use-operation-end", dest="use_operation_end", action='store_true',
                        help="Use the last day of operation (last day with complete data) "
                             + "as end date for data retrieval. "
                             + "If set, -e is ignored.  Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-o", "--output-folder", dest="output_folder", action='store', default='.', type=str,
                        help="Path to the output folder for the json data files. Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-R", "--refresh-data", dest = "refresh_data", action = 'store_true',
                        help="Fetch data even if data file exists. Ignored if A, -D, -O or -P is used.")
    parser.add_argument("-m", "--max-fetch-attempt", dest="max_fetch_attempt", default="20", action='store', type=int,
                        help="Maximum number of data fetch attempts.")
    parser.add_argument("user_data_file",
                        action="store", default='solaredge_userdata.toml', nargs="?", metavar="USERDATAFILE",
                        help="Path to the file (TOML) containing the user data (API key and Site ID). " +
                        "default: expects to use the file solaredge_userdata.toml in the script folder.")

    args = parser.parse_args()

    # argument post-processing
    if args.start_date is None:
        args.start_date = args.end_date
    for k in [args.start_date, args.end_date]:
        try:
            date.fromisoformat(k)
        except ValueError as err:
            print("ERROR: start/stop date error: ", err)
            return 1
    args.max_fetch_attempt = str(max(1,int(args.max_fetch_attempt)))

    # init solaredge installation
    se_mon = SolarEdgeMonitor(args.user_data_file, args.output_folder)
    if se_mon.is_error():
        print(se_mon.get_error())
        return 1

    if args.admin_info:
        admin_info = se_mon.get_admin_info()
        if se_mon.is_error():
            print(se_mon.get_error())
            return 1
        else:
            return 0

    if args.data_period:
        se_mon.get_data_period(True)
        if se_mon.is_error():
            print(se_mon.get_error())
            return 1
        else:
            return 0

    if args.overview:
        se_mon.get_overview_power_production()
        if se_mon.is_error():
            print(se_mon.get_error())
            return 1
        else:
            return 0

    if args.current_power:
        se_mon.get_current_power_flow()
        if se_mon.is_error():
            print(se_mon.get_error())
            return 1
        else:
            return 0

    if se_mon.get_data(args.start_date, args.period, args.use_operation_start ,args.end_date, args.use_operation_end,
                       args.refresh_data, args.max_fetch_attempt, args.quiet):
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(solaredgemon())
