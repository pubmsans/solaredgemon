# solaredgemon

Command line tools for fetching solardege inverter monitoring data from solaredge servers. API key and Site ID required.

## Setup

* The [python API module from SolarEdge](https://pypi.org/project/solaredge/)
* [Pandas](https://pandas.pydata.org/)

Installation via `pip` in a virtual environment:

    pip install solaredge pandas tomli

## Usage

    usage: solaredgemon.py [options] [USERDATAFILE]

    solaredgemon.py is a wrapper for solaredge module
    
    positional arguments:
      USERDATAFILE          Path to the file (TOML) containing the user data (API key and Site ID). default: expects to use the file solaredge_userdata.toml in the script folder.
    
    options:
      -h, --help            show this help message and exit
      -q, --quiet           Quiet mode.
      -A, --admin-info      Get admin info
      -D, --data-period     Get data period. Ignored if -A is used.
      -O, --overview-production
                            Get power production overview. Ignored if -A, or -O is used.
      -P, --current-power   Get current power flow. Ignored if -A,-D or -O is used.
      -s START_DATE, --start-date START_DATE
                            Start date for data retrieval (included). Date format is YYYY-MM-DD. Ignored if -A, -D, -O or -P is used.
      -p PERIOD, --period PERIOD
                            Integer number of day that defines the period for data retrieval. The start date is defined by subtracting this number from end date.If defined, -s is ignored. Ignored if A, -D, -O or -P is used.
      -S, --use-operation-start
                            Use the first day of operation (first day with complete data) as start date for data retrieval. If set, -s and -p are ignored. Ignored if A, -D, -O or -P is used.
      -e END_DATE, --end-date END_DATE
                            End date for data retrieval (included). Date format is YYYY-MM-DD. Ignored if A, -D, -O or -P is used.
      -E, --use-operation-end
                            Use the last day of operation (last day with complete data) as end date for data retrieval. If set, -e is ignored. Ignored if A, -D, -O or -P is used.
      -o OUTPUT_FOLDER, --output-folder OUTPUT_FOLDER
                            Path to the output folder for the json data files. Ignored if A, -D, -O or -P is used.
      -R, --refresh-data    Fetch data even if data file exists. Ignored if A, -D, -O or -P is used.
      -m MAX_FETCH_ATTEMPT, --max-fetch-attempt MAX_FETCH_ATTEMPT
                            Maximum number of data fetch attempts.


## User Data File (TOML)

This file stores the personal API key and the Site ID:

```toml
apikey = "YOURSOLAREDGEAPIKEY"
siteid = 1234567
```
